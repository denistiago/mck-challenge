package com.denistiago.reporting.model;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class Reports {
  private List<Report> elements;
  private Long total;
  private Integer page;
  private Integer totalPages;
}
