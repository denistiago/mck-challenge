package com.denistiago.reporting.model;

import java.util.Date;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "reports")
@Data
@Accessors(chain = true)
public class Report {

  @Id private String id;

  private String source;
  private String sourceIdentityId;
  private ReportReference reference;
  private State state = State.OPEN;
  private Date created;
  private ReportPayload payload;

  public enum State {
    OPEN,
    BLOCKED,
    RESOLVED
  }
}
