package com.denistiago.reporting.model;

import lombok.Data;

@Data
public class ReportPayload {
  private String source;
  private String reportType;
  private String message;
  private String reportId;
  private String referenceResourceId;
  private String referenceResourceType;
}
