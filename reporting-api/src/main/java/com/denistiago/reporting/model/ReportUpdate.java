package com.denistiago.reporting.model;

import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class ReportUpdate {

  @NotNull private Report.State ticketState;
}
