package com.denistiago.reporting.model;

import lombok.Data;

@Data
public class ReportReference {
  private String referenceId;
  private String referenceType;
}
