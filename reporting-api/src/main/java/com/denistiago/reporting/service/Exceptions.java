package com.denistiago.reporting.service;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

public class Exceptions {
  public static RuntimeException reportNotFound(String reportId) {
    return new NotFoundException(String.format("report %s not found", reportId));
  }

  @ResponseStatus(HttpStatus.NOT_FOUND)
  static class NotFoundException extends RuntimeException {
    public NotFoundException(String message) {
      super(message);
    }
  }
}
