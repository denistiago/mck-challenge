package com.denistiago.reporting.service;

import com.denistiago.reporting.model.Report;
import com.denistiago.reporting.model.Report.State;
import com.denistiago.reporting.model.Reports;
import com.denistiago.reporting.repository.ReportRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ReportingService {

  private final ReportRepository repository;

  public Reports getOpenReports(Integer page, Integer size) {
    Page<Report> reportPage = repository.findByState(State.OPEN, PageRequest.of(page, size));
    return new Reports(
        reportPage.getContent(), reportPage.getTotalElements(), page, reportPage.getTotalPages());
  }

  public Report updateState(String reportId, State state) {
    Report report = getReport(reportId);
    report.setState(state);
    repository.save(report);
    return report;
  }

  private Report getReport(String reportId) {
    return repository.findById(reportId).orElseThrow(() -> Exceptions.reportNotFound(reportId));
  }
}
