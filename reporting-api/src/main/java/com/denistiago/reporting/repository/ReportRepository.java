package com.denistiago.reporting.repository;

import com.denistiago.reporting.model.Report;
import com.denistiago.reporting.model.Report.State;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReportRepository extends MongoRepository<Report, String> {
  Page<Report> findByState(State state, Pageable pageable);
}