package com.denistiago.reporting;

import com.denistiago.reporting.model.Reports;
import com.denistiago.reporting.repository.ReportRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.InputStream;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.io.Resource;

@SpringBootApplication
@AllArgsConstructor
public class Application implements CommandLineRunner {

  @Autowired ReportRepository repository;
  @Autowired ObjectMapper mapper;

  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }

  @Override
  public void run(String... args) throws Exception {

    if (repository.count() == 0) {
      InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream("data.json");
      Reports reports = mapper.readValue(resourceAsStream, Reports.class);
      reports
          .getElements()
          .forEach(
              report -> repository.save(report));
    }
  }
}
