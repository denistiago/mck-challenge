package com.denistiago.reporting.controller;

import com.denistiago.reporting.model.Report;
import com.denistiago.reporting.model.ReportUpdate;
import com.denistiago.reporting.model.Reports;
import com.denistiago.reporting.service.ReportingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping(value = "/api/reports", produces = MediaType.APPLICATION_JSON_VALUE)
public class ReportingController {

  private final ReportingService reportingService;

  @GetMapping
  public Reports get(
      @RequestParam(defaultValue = "0") Integer page,
      @RequestParam(defaultValue = "10") Integer size) {
    return reportingService.getOpenReports(page, size);
  }

  @ResponseBody
  @PutMapping(value = "/{reportId}", consumes = MediaType.APPLICATION_JSON_VALUE)
  public Report update(
      @PathVariable String reportId, @RequestBody @Validated ReportUpdate reportUpdate) {
    return reportingService.updateState(reportId, reportUpdate.getTicketState());
  }
}
