package com.denistiago.reporting.controller;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.denistiago.reporting.model.Report;
import com.denistiago.reporting.model.Report.State;
import com.denistiago.reporting.model.Reports;
import com.denistiago.reporting.service.Exceptions;
import com.denistiago.reporting.service.ReportingService;
import java.util.List;
import lombok.SneakyThrows;
import lombok.val;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

public class ReportingControllerTest {

  ReportingService reportingServiceMock;
  ReportingController sut;

  MockMvc mvc;

  @BeforeEach
  public void before() {
    reportingServiceMock = Mockito.mock(ReportingService.class);
    sut = new ReportingController(reportingServiceMock);
    mvc = MockMvcBuilders.standaloneSetup(sut).build();
  }

  @Test
  @SneakyThrows
  public void testGetReportsWithDefaultPagingParams() {

    // given:
    when(reportingServiceMock.getOpenReports(Mockito.eq(1), Mockito.eq(2)))
        .thenReturn(
            new Reports()
                .setTotal(2L)
                .setElements(List.of(new Report().setId("1"), new Report().setId("2"))));

    // when:
    val result =
        this.mvc.perform(get("/api/reports?page=1&size=2").accept(MediaType.APPLICATION_JSON));

    // then:
    result
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.total").value(2L))
        .andExpect(jsonPath("$.elements.size()").value(2L))
        .andExpect(jsonPath("$.elements[0].id").value("1"))
        .andExpect(jsonPath("$.elements[1].id").value("2"));
  }

  @Test
  @SneakyThrows
  public void testUpdateReportState() {

    // when:
    this.mvc.perform(
        put("/api/reports/123")
            .contentType(MediaType.APPLICATION_JSON)
            .content("{\"ticketState\":\"RESOLVED\"}"));

    // then:
    verify(reportingServiceMock, times(1)).updateState("123", State.RESOLVED);
  }

  @Test
  @SneakyThrows
  public void testUpdateReportState_ticketStateIsMandatory() {

    // when:
    val result =
        this.mvc.perform(
            put("/api/reports/123").contentType(MediaType.APPLICATION_JSON).content("{}"));

    // then:
    result.andExpect(status().isBadRequest());
    verifyNoInteractions(reportingServiceMock);
  }

  @Test
  @SneakyThrows
  public void testNotFoundErrorMapping() {

    // given:
    Mockito.when(reportingServiceMock.updateState(Mockito.anyString(), Mockito.any()))
        .thenThrow(Exceptions.reportNotFound("123"));

    // when:
    val result =
        this.mvc.perform(
            put("/api/reports/123")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"ticketState\":\"RESOLVED\"}"));

    // then:
    result.andExpect(status().isNotFound());
  }
}
