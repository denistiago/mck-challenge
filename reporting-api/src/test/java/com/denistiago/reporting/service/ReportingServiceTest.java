package com.denistiago.reporting.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;

import com.denistiago.reporting.model.Report;
import com.denistiago.reporting.model.Report.State;
import com.denistiago.reporting.model.Reports;
import com.denistiago.reporting.repository.ReportRepository;
import com.denistiago.reporting.service.Exceptions.NotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

class ReportingServiceTest {

  private ReportRepository reportRepositoryMock;
  private ReportingService sut;

  @BeforeEach
  public void before() {
    reportRepositoryMock = Mockito.mock(ReportRepository.class);
    sut = new ReportingService(reportRepositoryMock);
  }

  @Test
  public void testGetReports() {

    // given:
    List<Report> elements = new ArrayList<>();
    Long total = 0L;

    Mockito.when(reportRepositoryMock.findByState(State.OPEN, PageRequest.of(0, 10)))
        .thenReturn(new PageImpl<>(elements));

    // when:
    Reports reports = sut.getOpenReports(0, 10);

    // then:
    assertEquals(elements, reports.getElements());
    assertEquals(reports.getTotal(), total);
  }

  @Test
  public void testUpdateState() {

    // given:
    Mockito.when(reportRepositoryMock.findById("123")).thenReturn(Optional.of(new Report()));

    // when:
    Report updated = sut.updateState("123", State.RESOLVED);

    // then:
    assertEquals(State.RESOLVED, updated.getState());
    verify(reportRepositoryMock, Mockito.times(1)).save(updated);
  }

  @Test
  public void testUpdateState_notFoundReport() {

    // given:
    Mockito.when(reportRepositoryMock.findById("123")).thenReturn(Optional.empty());

    // then:
    Assertions.assertThrows(NotFoundException.class, () -> sut.updateState("123", State.RESOLVED));
  }
}
