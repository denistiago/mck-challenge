# building and running

[Docker](https://gitlab.com/denistiago/mck-challenge) is required to build and run the solution.

```sh
docker-compose build
docker-compose up
```

Application will run at http://localhost:3000

![](demo.mp4)
