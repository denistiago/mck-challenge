import axios from "axios";

const basePath = 'http://localhost:8080/api/';

export const getReports = (page, size) =>
  axios.get(`${basePath}/reports?page=${page}&size=${size}`, {});

export const updateReport = (id, state) =>
  axios.put(`${basePath}/reports/${id}`, {
  	"ticketState": state
  },{});