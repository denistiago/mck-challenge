import React from 'react';
import ReactDOM from 'react-dom';
import Reports from './Reports';

ReactDOM.render(<Reports />, document.getElementById('root'));
