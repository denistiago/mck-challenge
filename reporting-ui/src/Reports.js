import React, { Component } from 'react';
import {getReports} from './api.js';
import Report from './Report';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Pagination from '@material-ui/lab/Pagination';

class Reports extends Component {

  constructor(props) {
    super(props);
    this.state = {
      reports: [],
      page: 0,
      totalPages: 0
    };
    this.loadReports = this.loadReports.bind(this);
  }

  componentDidMount() {
    this.loadReports()
  }

  loadReports(p, s) {
    
    var page = p || 0;
    var size = s || 10;

    getReports(page, size)
    .then(response => {      
      this.setState({ reports: response.data.elements, page: response.data.page, totalPages: response.data.totalPages });
    });
  }

  render() {    
    return (
      <React.Fragment>
        <CssBaseline />
        <Container maxWidth="md">        
          
          {this.state.reports.map((report, index) => (
            <Report key={report.id} report={report} loadReports={this.loadReports} />
          ))}        
          
          <Pagination count={this.state.totalPages} 
                      page={this.state.page + 1} 
                      onChange={(e, page) => this.loadReports(page - 1)} />
        </Container>
      </React.Fragment>
    );
  }
}

export default Reports;