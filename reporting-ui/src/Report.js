import React, { Component } from 'react';
import {updateReport} from './api.js';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

class Report extends Component {

  constructor(props) {
    super(props);
    this.state = {
      report: props.report
    };
  }

  setReportState(report, state, e) {
    e.preventDefault();
   updateReport(report.id, state).then(response => {
      report.state = state;
      this.setState({report: report});
      this.props.loadReports();
   }); 
  }

  render() {
    return (      
      <div>
        <Grid container spacing="2">
          <Grid item xs={3}>
            <div>
            <Typography gutterBottom>
              {`ID: ${this.state.report.id}`}
            </Typography>                          
            </div>
            <div>
            <Typography gutterBottom>
              {`Type: ${this.state.report.payload.reportType}`}
            </Typography>                                        
            </div>
          </Grid>
          <Grid item xs={3}>
            <div>
              <Typography gutterBottom>
                {`State: ${this.state.report.state}`}
              </Typography>                                              
            </div>
            <div>
              <Typography gutterBottom>
                {`Message: ${this.state.report.payload.message||''}`}
              </Typography>                                                                          
            </div>
          </Grid>
          <Grid item xs={3}>
            <Box component="div" m={1}>
              <Button variant="contained" color="primary" onClick={(e) => this.setReportState(this.state.report, 'BLOCKED' ,e)}>
                Block
              </Button>
            </Box>            
            
            <Box component="div" m={1}>
              <Button variant="contained" color="primary" onClick={(e) => this.setReportState(this.state.report, 'RESOLVED', e)}>
                Resolve
              </Button>
            </Box>            
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default Report;
